package dao;

import data.GroupChat;
import data.Message;
import data.User;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static dao.Database.close;

public class GroupChatDAO {

    private GroupChat getGroupChatFromResultSet(ResultSet resultSet){
        GroupChat groupChat = null;
        try{
            if(resultSet.next()){
                groupChat = new GroupChat();
                groupChat.setGroupChatId(resultSet.getInt("groupChatId"));
                groupChat.setGroupChatName(resultSet.getString("groupChatName"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        } finally {
            close(null, null, resultSet);
        }
        return groupChat;
    }

    private ArrayList<Message> getMesagesFromResultSet(ResultSet resultSet) {
        ArrayList<Message> messages = new ArrayList<>();
        try{
            while (resultSet.next()) {
                Message message = new Message();
                message.setGroupChatId(resultSet.getInt("groupChatId"));
                message.setMessageContent(resultSet.getString("messageContent"));
                message.setTimestamp(resultSet.getTimestamp("timestamp"));
                message.setUserId1(resultSet.getInt("userId1"));
                message.setMessageId(resultSet.getInt("messageId"));
                messages.add(message);
            }
        }catch (SQLException e){
            e.printStackTrace();
        } finally {
            close(null, null, resultSet);
        }
        return messages;
    }

    private ArrayList<User> getUsersFromResultSet(ResultSet resultSet) {
        ArrayList<User> users = new ArrayList<>();
        UserDAO userDao = new UserDAO();
        try{
            while (resultSet.next()) {
                User user = userDao.getUserById(resultSet.getInt("userId"));
                users.add(user);
            }
        }catch (SQLException e){
            e.printStackTrace();
        } finally {
            close(null, null, resultSet);
        }
        return users;
    }

    private ArrayList<GroupChat> getGroupChatsFromUserIdResultSet(ResultSet resultSet) {
        ArrayList<GroupChat> groupChats = new ArrayList<>();

        try{
            while (resultSet.next()) {
                int groupChatId = resultSet.getInt("groupChatId");
                GroupChat groupChat = getGroupChat(groupChatId);
                groupChats.add(groupChat);
            }
        }catch (SQLException e){
            e.printStackTrace();
        } finally {
            close(null, null, resultSet);
        }
        return groupChats;
    }

    //Made so that the rest of the application runs without group chats implemented.
    public ArrayList<User> getGroupChatUsers(int groupChatId){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<User> users = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM user_groupChat WHERE groupChatId=?");
            preparedStatement.setInt(1, groupChatId);
            resultSet = preparedStatement.executeQuery();
            users = getUsersFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            close(connection, preparedStatement, resultSet);
        }
        return users;

    }



    public GroupChat addGroupChat(GroupChat groupChat) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(
                    "INSERT INTO groupChat (groupChatName) VALUES (?)", Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, groupChat.getGroupChatName());
            int result = preparedStatement.executeUpdate();

            if (result == 1) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) groupChat.setGroupChatId(resultSet.getInt(1));
            }
        }catch (SQLIntegrityConstraintViolationException e){
            groupChat = new GroupChat();
            return groupChat;
        } catch (SQLException e) {
            groupChat = null;
            e.printStackTrace();
        }finally{
            close(connection, preparedStatement, resultSet);
        }
        return groupChat;
    }


    public GroupChat getGroupChat(int groupChatId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        GroupChat groupChat = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM groupChat WHERE groupChatId=?");
            preparedStatement.setInt(1, groupChatId);
            resultSet = preparedStatement.executeQuery();
            groupChat = getGroupChatFromResultSet(resultSet);

            groupChat.setMessageList(getGroupChatMessages(groupChatId));
            groupChat.setUserList(getGroupChatUsers(groupChatId));
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            close(connection, preparedStatement, resultSet);
        }
        return groupChat;

    }


    public ArrayList<GroupChat> getGroupChatByUserId(int userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<GroupChat> groupChats = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM user_groupChat WHERE userId=?");
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            groupChats = getGroupChatsFromUserIdResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            close(connection, preparedStatement, resultSet);
        }
        return groupChats;
    }

    public ArrayList<Message> getGroupChatMessages(int groupChatId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Message> messages = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM message WHERE groupChatId=?");
            preparedStatement.setInt(1, groupChatId);
            resultSet = preparedStatement.executeQuery();
            messages = getMesagesFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            close(connection, preparedStatement, resultSet);
        }
        return messages;
    }



    public Message addMessage(Message message) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(
                    "INSERT INTO message (userId1, groupChatId, timestamp, messageContent) VALUES (?, ?, NOW(), ?)", Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, message.getUserId1());
            preparedStatement.setInt(2, message.getGroupChatId());
            preparedStatement.setString(3, message.getMessageContent());

            int result = preparedStatement.executeUpdate();

            if (result == 1) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) message.setMessageId(resultSet.getInt(1));
            }
        }catch (SQLIntegrityConstraintViolationException e){
            message = new Message();
            return message;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            close(connection, preparedStatement, resultSet);
        }
        return message;
    }
}
