package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        final String errorMessage = "Du tastet inn noe feil";

        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");
        ArrayList<Integer> numbers;
        try {
            numbers = new ArrayList<>(Arrays.stream(expressionTrimmed.split("[\\+\\-\\*\\/]")).map(Integer::parseInt).collect(Collectors.toList()));
        } catch (NumberFormatException e) {
            return errorMessage;
        }
        int result = -1;

        ArrayList<Character> signs = new ArrayList<>();
        for (int i = 0; i < expressionTrimmed.length(); ++i) {
            char ch = expressionTrimmed.charAt(i);
            if (ch == '-' || ch == '+' || ch == '/' || ch == '*') {
                signs.add(ch);
            }
        }

        try {
            // Ganging / deling
            for (int i = 0; i < signs.size();) {
                switch (signs.get(i)) {
                    case '*' :{
                        numbers.set(i, numbers.get(i) * numbers.get(i + 1));
                        numbers.remove(i + 1);
                        signs.remove(i);
                        break;
                    }
                    case '/': {
                        numbers.set(i, numbers.get(i) / numbers.get(i + 1));
                        numbers.remove(i + 1);
                        signs.remove(i);
                        break;
                    }
                    default: {
                        ++i;
                        break;
                    }
                }
            }

            // Pluss / minus
            for (int i = 0; i < signs.size();) {
                switch (signs.get(i)) {
                    case '+' :{
                        numbers.set(i, numbers.get(i) + numbers.get(i + 1));
                        numbers.remove(i + 1);
                        signs.remove(i);
                        break;
                    }
                    case '-': {
                        numbers.set(i, numbers.get(i) - numbers.get(i + 1));
                        numbers.remove(i + 1);
                        signs.remove(i);
                        break;
                    }
                    default: {
                        return errorMessage;
                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            return errorMessage;
        }


        if (numbers.size() != 1) {
            return errorMessage;
        } else {
            return numbers.get(0).toString();
        }
    }

}
