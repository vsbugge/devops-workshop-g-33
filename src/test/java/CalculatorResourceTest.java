import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals("6", calculatorResource.calculate("1+2+3"));
        assertEquals("2011", calculatorResource.calculate("9+2+2000"));
        assertEquals("Du tastet inn noe feil", calculatorResource.calculate("200++3"));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals("2", calculatorResource.calculate("4-2"));
        assertEquals("Du tastet inn noe feil", calculatorResource.calculate("jeiugheiwa"));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals("8", calculatorResource.calculate("4*2"));
        assertEquals("6", calculatorResource.calculate("3*2*1"));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals("2", calculatorResource.calculate("22/11"));
        assertEquals("2", calculatorResource.calculate("13/5"));
    }

    @Test
    public void testMix() {
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals("10",calculatorResource.calculate("2 + 3 * 6 - 10"));
    }
}
